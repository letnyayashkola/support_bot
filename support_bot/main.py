import logging
import os
from functools import wraps

from dotenv import load_dotenv

import emoji

import filters

import redis

import redisp

from telegram import Bot, InlineKeyboardButton, InlineKeyboardMarkup, Message
from telegram.callbackquery import CallbackQuery
from telegram.ext import (
    BasePersistence,
    CallbackContext,
    CallbackQueryHandler,
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
)
from telegram.update import Update as StateUpdate

import ujson

__version__ = "0.3.4"

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

WELCOME_TEXT = (
    "Привет, летнешкольники! Сезон ЛШ-2019 уже закончился, но мы "
    + "решили протестировать бот обратной связи. Сюда можно писать с любыми "
    + "вопросами / откликами / предложениями, причем в анонимном режиме или "
    + "представившись.\n\nБот работает в экспериментальном режиме. "
    + "Мы хотим оценить его востребованность и спектр вопросов, которые "
    + "удобнее решать онлайн.\n\nЕсли вы представитесь, то мы сможем вам"
    + " ответить.\nЕсли вы напишете анонимно, то мы примем к сведению, но "
    + "не сможем отозваться — потому что не будем знать, кому.\n\nПо умолчанию "
    + " анонимный режим выключен\nДля включения анонимного режима работает"
    + " команда /anon_on\nЧтобы вернуться к возможности написать что-либо "
    + "от своего имени, используйте команду /anon_off\n\nСпасибо за этот "
    + "сезон,\nМы все уехали, мы все остались здесь\n\n/\\\\\\ 2019"
)

EMOJIES = {
    "up": emoji.emojize(":thumbsup:", use_aliases=True),
    "down": emoji.emojize(":thumbsdown:", use_aliases=True),
    "up_checked": emoji.emojize(":thumbsup: :white_check_mark:", use_aliases=True),
    "down_checked": emoji.emojize(":thumbsdown: :white_check_mark:", use_aliases=True),
}

LOG = logging.getLogger(__name__)

try:
    load_dotenv()
except OSError:
    LOG.info(".env file not found")


def build_reply_kb(first_action: str, second_action: str, msg_id: int):
    return InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    EMOJIES[first_action],
                    callback_data=ujson.dumps(
                        {"action": first_action, "msg_id": msg_id}
                    ),
                ),
                InlineKeyboardButton(
                    EMOJIES[second_action],
                    callback_data=ujson.dumps(
                        {"action": second_action, "msg_id": msg_id}
                    ),
                ),
            ]
        ]
    )


def check_ban(func):
    @wraps(func)
    def wrapped(self, update: StateUpdate, context: CallbackContext, *args, **kwargs):
        if "banned" not in context.user_data:
            context.user_data["banned"] = False

        if context.user_data["banned"]:
            text = "Прости дружище, я на тебя обиделся"
            if update.message:
                update.message.reply_text(text)
            else:
                update.callback_query.answer(text=text)
            return
        return func(self, update, context, *args, **kwargs)

    return wrapped


def check_can_send_reply(func):
    @wraps(func)
    def wrapped(self, update: StateUpdate, context: CallbackContext, *args, **kwargs):
        if not update.message.reply_to_message.forward_date:
            update.message.reply_text("И что мне с этим делать?")
            return

        return func(self, update, context, *args, **kwargs)

    return wrapped


class Handlers:
    def __init__(self, redis: redis.Redis, support_chat_id: int):
        self.support_chat_id = support_chat_id
        self._redis = redis

    def start(self, update: StateUpdate, context: CallbackContext):
        context.user_data["is_anon"] = context.user_data.get("is_anon", False)
        context.user_data["banned"] = context.user_data.get("banned", False)

        update.message.reply_text(WELCOME_TEXT)

    def help_(self, update: StateUpdate, context: CallbackContext):
        update.message.reply_text(WELCOME_TEXT)

    def whoisyourdaddy(self, update: StateUpdate, context: CallbackContext):
        update.message.reply_text(
            f"IT-изба, 2019\nВерсия: {__version__}\n"
            + "Source code: https://gitlab.com/letnyayashkola/support_bot"
        )

    def anon_on(self, update: StateUpdate, context: CallbackContext):
        context.user_data["is_anon"] = True

        update.message.reply_text("Анонимный режим включен")

    def anon_off(self, update: StateUpdate, context: CallbackContext):
        context.user_data["is_anon"] = False

        update.message.reply_text("Анонимный режим выключен")

    def _get_chat_id(self, message: Message) -> int:
        if message.reply_to_message.forward_from:
            return message.reply_to_message.forward_from.id
        else:
            return int(
                self._redis.hget("messages", message.reply_to_message.message_id)
            )

    @check_can_send_reply
    def handle_reply_in_group(self, update: StateUpdate, context: CallbackContext):
        chat_id = self._get_chat_id(update.message)

        self.send_copy_message(
            context.bot,
            update.message,
            chat_id,
            reply_markup=build_reply_kb("up", "down", update.message.message_id),
        )

        update.message.reply_text("Отправлено")

    def send_copy_message(
        self, bot: Bot, msg: Message, to_chat: int, reply_markup=None
    ) -> Message:

        if msg.text:
            return bot.send_message(to_chat, msg.text, reply_markup=reply_markup)
        if msg.photo:
            photos: list = msg.photo
            photos.sort(key=lambda x: x["file_size"])  # send max size photo
            return bot.send_photo(
                to_chat, photos[-1], caption=msg.caption, reply_markup=reply_markup
            )
        if msg.sticker:
            return bot.send_sticker(to_chat, msg.sticker, reply_markup=reply_markup)
        if msg.voice:
            return bot.send_voice(
                to_chat, msg.voice, caption=msg.caption, reply_markup=reply_markup
            )
        if msg.audio:
            return bot.send_audio(to_chat, msg.audio, reply_markup=reply_markup)
        if msg.video:
            return bot.send_video(to_chat, msg.video, reply_markup=reply_markup)
        if msg.video_note:
            return bot.send_video_note(
                to_chat, msg.video_note, reply_markup=reply_markup
            )
        if msg.document:
            return bot.send_document(to_chat, msg.document, reply_markup=reply_markup)
        if msg.animation:
            return bot.send_animation(to_chat, msg.animation, reply_markup=reply_markup)
        if msg.contact:
            return bot.send_contact(to_chat, msg.contact, reply_markup=reply_markup)
        if msg.game:
            return bot.send_game(to_chat, msg.game, reply_markup=reply_markup)
        if msg.location:
            return bot.send_location(to_chat, msg.location, reply_markup=reply_markup)
        if msg.venue:
            return bot.send_venue(to_chat, msg.venue, reply_markup=reply_markup)

        LOG.info("msg type not found: ", msg.to_dict())
        return None

    @check_ban
    def handle_msg(self, update: StateUpdate, context: CallbackContext):
        if "is_anon" not in context.user_data:
            context.user_data["is_anon"] = False

        if not context.user_data["is_anon"]:
            res = context.bot.forward_message(
                chat_id=self.support_chat_id,
                from_chat_id=update.message.chat_id,
                message_id=update.message.message_id,
            )
        else:
            res = self.send_copy_message(
                context.bot, update.message, self.support_chat_id
            )

            update.message.reply_text(
                "Отправлено. Просто напомню, что у тебя включен анонимный "
                + "режим. На твое сообщение не смогут ответить. Чтобы "
                + "выключить, отправь команду /anon_off"
            )

        self._redis.hset("messages", res.message_id, update.message.chat_id)

    def unknown_command(self, update: StateUpdate, context: CallbackContext):
        update.message.reply_text("Я не понимаю")

    @staticmethod
    def _ban_action(
        user_data: dict, persis: BasePersistence, user_id: int, banned: bool
    ):
        user_data[user_id]["banned"] = banned
        persis.update_user_data(user_id, user_data[user_id])

    def ban_user(self, user_data: dict, persis: BasePersistence):
        def _ban_user(update: StateUpdate, context: CallbackContext):
            chat_id = self._get_chat_id(update.message)
            self._ban_action(user_data, persis, chat_id, True)

            update.message.reply_text("Выпелен")

        return _ban_user

    def unban_user(self, user_data: dict, persis: BasePersistence):
        def _unban_user(update: StateUpdate, context: CallbackContext):
            chat_id = self._get_chat_id(update.message)
            self._ban_action(user_data, persis, chat_id, False)

            update.message.reply_text("Реабилитирован")

        return _unban_user

    @check_ban
    def inline_button_event(self, update: StateUpdate, context: CallbackContext):
        query: CallbackQuery = update.callback_query

        data = ujson.loads(query.data)

        action = data["action"]

        if action.endswith("checked"):
            query.answer(text="Уже выбрано")
            return

        context.bot.send_message(
            self.support_chat_id,
            "Реакция: " + EMOJIES[action],
            reply_to_message_id=data["msg_id"],
        )

        query.answer(text="Готово")

        if action == "up":
            query.edit_message_reply_markup(
                build_reply_kb("up_checked", "down", data["msg_id"])
            )

        if action == "down":
            query.edit_message_reply_markup(
                build_reply_kb("up", "down_checked", data["msg_id"])
            )

    def error_handler(self, update: StateUpdate, context: CallbackContext):
        data: dict = {}
        if update and update.message:
            data = update.message.to_dict()

        LOG.error("Error while processing update: %s, %s", data, context.error)


def request_kwargs(proxy):
    kwargs: dict = {"read_timeout": 10, "connect_timeout": 10}

    if proxy is not None:
        kwargs["proxy_url"] = proxy

    return kwargs


def main():
    token = os.getenv("SUPPORT_BOT_TOKEN", "")

    url = os.getenv("SUPPORT_BOT_URL", "")
    port = int(os.getenv("SUPPORT_BOT_PORT", "5002"))

    proxy = os.getenv("SUPPORT_BOT_PROXY", "")

    support_chat_id = int(os.getenv("SUPPORT_BOT_CHAT_ID", "0"))

    redis_url = os.getenv("SUPPORT_BOT_REDIS_URL", "redis://localhost")

    if not support_chat_id:
        LOG.error("SUPPORT_BOT_CHAT_ID required")
        return

    LOG.info("Start support bot, version: %s", __version__)
    LOG.info({k: v for k, v in os.environ.items() if k.startswith("SUPPORT_BOT")})

    if not token:
        LOG.error("bot token not found")
        return

    client = redis.from_url(redis_url)
    persist = redisp.RedisPersistence(client)

    updater = Updater(
        token=token,
        persistence=persist,
        use_context=True,
        request_kwargs=request_kwargs(proxy),
    )

    from_support_chat = Filters.chat(chat_id=support_chat_id) & filters.reply_to_bot

    dispatcher = updater.dispatcher

    handlers = Handlers(client, support_chat_id)

    dispatcher.add_handler(CommandHandler("start", handlers.start, ~Filters.group))
    dispatcher.add_handler(CommandHandler("help", handlers.help_))
    dispatcher.add_handler(CommandHandler("whoisyourdaddy", handlers.whoisyourdaddy))
    dispatcher.add_handler(CommandHandler("anon_on", handlers.anon_on, ~Filters.group))
    dispatcher.add_handler(
        CommandHandler("anon_off", handlers.anon_off, ~Filters.group)
    )
    dispatcher.add_handler(
        CommandHandler(
            "ban", handlers.ban_user(dispatcher.user_data, persist), from_support_chat
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            "unban",
            handlers.unban_user(dispatcher.user_data, persist),
            from_support_chat,
        )
    )
    dispatcher.add_handler(MessageHandler(Filters.command, handlers.unknown_command))
    dispatcher.add_handler(
        MessageHandler(from_support_chat, handlers.handle_reply_in_group)
    )

    dispatcher.add_handler(
        MessageHandler(Filters.all & ~Filters.group, handlers.handle_msg)
    )

    dispatcher.add_handler(CallbackQueryHandler(handlers.inline_button_event))
    dispatcher.add_error_handler(handlers.error_handler)

    if url and port:
        updater.start_webhook(listen="0.0.0.0", port=port, url_path=token)

        url = url if not url.endswith("/") else url[:-1]

        updater.bot.set_webhook(url=f"{url}/{token}")

        LOG.info("URL: %s", f"{url}/{token}")
        LOG.info("Port: %s", port)
    else:
        updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    main()
