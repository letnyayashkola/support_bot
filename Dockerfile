FROM inn0kenty/pyinstaller-alpine:3.7 as builder

WORKDIR /build

RUN pip install poetry

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.create false \
    && poetry install --no-root --no-dev --no-interaction

COPY . .

RUN pyinstaller --noconfirm --clean --onefile --name app support_bot/main.py

FROM alpine:3.11

RUN addgroup -g 1000 -S app \
    && adduser -S -u 1000 -G app app \
    && mkdir /storage \
    && chown -R app:app /storage

LABEL maintainer="Innokenty Lebedev <i.lebedev@letnyayashkola.org>"

COPY --from=builder --chown=1000:1000 /build/dist/app .

USER app

ENTRYPOINT ["./app"]
